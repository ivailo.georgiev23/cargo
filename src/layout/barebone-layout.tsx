import React from "react";

// Importing all created components
import { Box } from "@mui/material";

// Pass the child props
export default function BareboneLayout({
  children,
}: {
  children: React.ReactElement;
}) {
  return (
    <Box display="flex" flexDirection="column" flexGrow={1} justifyContent="center" alignContent="center">
      {children}
    </Box>
  );
}
