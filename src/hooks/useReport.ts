import { reportNRA } from "../api";
import { reportNraRequest } from "../ts/types/reportNraRequest";
import { useSessionStorage } from "./useSessionStorage";
import { nraReportsResponseData } from "../ts/interfaces/nraReportDataType";

export const useReport = () => {
  const { getItem } = useSessionStorage();

  const getReportNra = async (
    params: reportNraRequest,
  ): Promise<nraReportsResponseData | undefined> => {
    const sessionStorage: string | null = getItem("token");

    const response: nraReportsResponseData | undefined = await reportNRA(
      params,
      sessionStorage,
    );
    return response;
  };

  return { getReportNra };
};
