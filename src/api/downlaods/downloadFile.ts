import Axios from "axios";

const baseUrl = "/v1/report/nra";
const downloadUrl = `${baseUrl}`;

import { reportRequst } from "../../ts/types/reportNraRequest";

export const downloadFile = async (url: string, token: string | null, params: reportRequst) => {
  try {
    const response = await Axios.post(
      `${downloadUrl}/${url}`,
      params,
      { headers: { Authorization: `Bearer ${token}` } },
    );

    return response;
  } catch (error) {
    return error;
  }
};
