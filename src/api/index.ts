// import Axios from "axios";
// import { API_TOKEN_NAME } from "./globals";
// import { intercept } from "../interceptors/intercept";
// import { handleErrors } from "../interceptors/handleErrors";

// if (token) Axios.defaults.headers.common["Authorization"] = token;

/**
 * User
 */
export { loginApi } from "./user/loginApi";

/**
 * Reports
 */
export { reportNRA } from "./reports/reportNRA";

/**
 * Downloads
 */

export { downloadFile } from './downlaods/downloadFile'