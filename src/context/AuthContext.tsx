import { createContext, ReactNode, useEffect, useState } from "react";
import { useSessionStorage } from "../hooks/useSessionStorage";
type Props = {
  children?: ReactNode;
};

type IAuthContext = {
  isLoggedIn: boolean;
  isLoading: boolean;
  jwt: string;
  login: (token: string) => void;
  logout: () => void;
};

const initialValue = {
  isLoggedIn: false,
  isLoading: false,
  jwt: "",
  login: () => {},
  logout: () => {},
};

const AuthContext = createContext<IAuthContext>(initialValue);

const AuthProvider = ({ children }: Props) => {
  const sessionStorage = useSessionStorage();
  //Initializing an auth state with false value (unauthenticated)
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [jwt, setJwt] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const storedJwt = sessionStorage.getItem("token");
    if (storedJwt) {
      setIsLoggedIn(true);
      setJwt(storedJwt);
    }
    setIsLoading(false);
  }, []);

  async function login(token: string) {
    setIsLoggedIn(true);
    setJwt(token);
    sessionStorage.setItem("token", token);
  }

  function logout() {
    setIsLoggedIn(false);
    setJwt("");
    sessionStorage.removeItem("token");
  }

  const authValue = {
    isLoggedIn,
    isLoading,
    jwt,
    login,
    logout,
  };
  return (
    <AuthContext.Provider value={authValue}>{children}</AuthContext.Provider>
  );
};

export { AuthContext, AuthProvider };
