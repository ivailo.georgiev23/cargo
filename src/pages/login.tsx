import { useContext, useState } from "react";
import BareboneLayout from "../layout/barebone-layout";
import { Box, Button, TextField, Typography } from "@mui/material";
import { AuthContext } from "../context/AuthContext";
import { Navigate } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";

function Login() {
  const login = useAuth();
  const { isLoggedIn } = useContext(AuthContext);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [code, setCode] = useState("");

  const handleLogin = () => {
    login.userLogin({
      username,
      password,
    });
  };

  if (isLoggedIn) {
    return <Navigate to="/" />;
  }

  return (
    <BareboneLayout>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "30rem" },
        }}
        noValidate
        autoComplete="off"
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <Typography variant="h4" component="h1" align="center">
          Cargo
        </Typography>
        <TextField
          id="outlined-basic"
          label="Username"
          variant="outlined"
          value={username}
          onChange={(event) => {
            setUsername(event.target.value);
          }}
        />
        <TextField
          id="filled-basic"
          type="password"
          label="Password"
          variant="outlined"
          value={password}
          onChange={(event) => {
            setPassword(event.target.value);
          }}
        />
        <TextField
          id="filled-basic"
          label="Code"
          variant="outlined"
          value={code}
          onChange={(event) => {
            setCode(event.target.value);
          }}
        />
        <Box display="flex" justifyContent="end">
          <Button variant="contained" size="large" onClick={handleLogin}>
            Login
          </Button>
        </Box>
      </Box>
    </BareboneLayout>
  );
}

export default Login;
