import Axios from "axios";
import { User } from "../../ts/types/userRequest";

const baseUrl = "";
const registerUrl = `${baseUrl}/login`;


export const loginApi = async (params: User) => {
  try {
    const response = await Axios.post(registerUrl, params);
    return response.data;
  } catch (error) {
    return error;
  }
};
