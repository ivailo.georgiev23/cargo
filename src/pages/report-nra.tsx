import React, { useEffect, useState } from "react";
import MainLayout from "../layout/main-layout";
import TableNRA from "../components/data-grid";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  InputLabel,
  Menu,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { useDownload } from "../hooks/useDownload";
import { useReport } from "../hooks/useReport";
import { reportNraRequest, reportRequst } from "../ts/types/reportNraRequest";
import { nraReportsResponseData } from "../ts/interfaces/nraReportDataType";

function ReportNRA() {
  const [period, setPeriod] = useState<number>(1);
  const [year, setYear] = useState<number>(2024);
  const [internal, setInternal] = useState<boolean>(false);
  const [international, setInternational] = useState<boolean>(false);
  const [cod, setCod] = useState<boolean>(false);

  const { downloadReport } = useDownload();
  const { getReportNra } = useReport();

  const [reportNraData, setReportNraData] = useState<nraReportsResponseData>();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const fetchReportData = async () => {
    const reportParams: reportNraRequest = {
      quarter: Number(period),
      year: Number(year),
      domestic: internal,
      international: international,
      cod: cod,
      page: 1,
      size: 25,
      sortBy: "",
      sortDir: "",
    };

    const reportData: nraReportsResponseData = (await getReportNra(
      reportParams,
    )) as nraReportsResponseData;

    console.log("reportData", reportData);

    setReportNraData(reportData);
  };

  useEffect(() => {
    fetchReportData();
  }, []);

  const handleDownload = async (filePath: string) => {
    const reportRequestData: reportRequst = {
      quarter: Number(period),
      year: Number(year),
      domestic: internal,
      international: international,
      cod: cod,
    };

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const response: any = await downloadReport(filePath, reportRequestData);

    if (response) {
      const fileData = response.data;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      const blob = new Blob([fileData], {type: "text/xml", endings: 'native'});
      // const blob = new Blob([fileData?.join(',\n')], {type: "text/plain", endings: 'native'});
      const url = URL.createObjectURL(blob);
      const link = document.createElement("a");
      const disposition = response.headers["content-disposition"];
      let filename = "";

      if (disposition && disposition.indexOf("attachment") !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, "");
        }
      }
      
      link.download = filename;
      link.href = url;
      link.click();
    }
  };
  const handleSearch = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    fetchReportData();
  };

  const handleReport = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleDownloadReportCod = async () => {
    await handleDownload("nppost");
  };
  const handleDownloadReportNRA = async () => {
    await handleDownload("mppost");
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleInterval = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target.checked);
    setInternal(event.target.checked);
  };

  const handleInternational = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target.checked);
    setInternational(event.target.checked);
  };
  const handleCOD = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target.checked);
    setCod(event.target.checked);
  };

  const handleChangePeriod = (event: SelectChangeEvent) => {
    setPeriod(Number(event.target.value) as number);
  };

  const handleChangeYear = (event: SelectChangeEvent) => {
    setYear(Number(event.target.value) as number);
  };

  return (
    <MainLayout>
      <>
        <Box component="form" noValidate autoComplete="off" display="flex">
          <Box>
            <FormControl required sx={{ m: 1, minWidth: 240 }}>
              <InputLabel id="demo-simple-select-required-label">
                Period
              </InputLabel>
              <Select
                labelId="demo-simple-select-required-label"
                id="demo-simple-select-required"
                value={period.toString()}
                label="Period *"
                onChange={handleChangePeriod}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={1}>Q1</MenuItem>
                <MenuItem value={2}>Q2</MenuItem>
                <MenuItem value={3}>Q3</MenuItem>
                <MenuItem value={4}>Q4</MenuItem>
              </Select>
              <FormHelperText>Required</FormHelperText>
            </FormControl>
            <FormControl required sx={{ m: 1, minWidth: 240 }}>
              <InputLabel id="demo-simple-select-required-label">
                Year
              </InputLabel>
              <Select
                labelId="demo-simple-select-required-label"
                id="demo-simple-select-required"
                value={year.toString()}
                label="Year *"
                onChange={handleChangeYear}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>

                <MenuItem value={2024}>2024</MenuItem>
                <MenuItem value={2023}>2023</MenuItem>
              </Select>
              <FormHelperText>Required</FormHelperText>
            </FormControl>
          </Box>

          <Box display="flex" flexWrap="wrap">
            <FormControlLabel
              value={internal}
              control={
                <Checkbox
                  onChange={(e) => {
                    handleInterval(e);
                  }}
                />
              }
              label="Internal"
              labelPlacement="end"
              sx={{ m: 1, maxWidth: 80 }}
            />
            <FormControlLabel
              value={international}
              control={
                <Checkbox
                  onChange={(e) => {
                    handleInternational(e);
                  }}
                />
              }
              label="International"
              labelPlacement="end"
              sx={{ m: 1, maxWidth: 120 }}
            />
            <FormControlLabel
              value={cod}
              control={
                <Checkbox
                  onChange={(e) => {
                    handleCOD(e);
                  }}
                />
              }
              label="COD"
              labelPlacement="end"
              sx={{ m: 1, maxWidth: 120 }}
            />
          </Box>
          <Box marginLeft="auto">
            <Button
              variant="contained"
              type="submit"
              sx={{ m: 1 }}
              onClick={handleSearch}
            >
              Search
            </Button>
            <Button
              id="basic-button"
              aria-controls={open ? "basic-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              onClick={handleReport}
              variant="contained"
            >
              Report
            </Button>
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
            >
              <MenuItem onClick={handleDownloadReportCod}>Report COD</MenuItem>
              <MenuItem onClick={handleDownloadReportNRA}>Report NRA</MenuItem>
            </Menu>
          </Box>
        </Box>
        {reportNraData && <TableNRA reportsData={reportNraData}></TableNRA>}
      </>
    </MainLayout>
  );
}

export default ReportNRA;
