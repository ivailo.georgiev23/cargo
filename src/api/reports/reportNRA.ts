import Axios from "axios";
import { reportNraRequest } from "../../ts/types/reportNraRequest";
import { nraReportsResponseData } from "../../ts/interfaces/nraReportDataType";

const baseUrl = "/v1/report/nra";
const reportrUrl = `${baseUrl}/grid`;

export const reportNRA = async (
  params: reportNraRequest,
  token: string | null,
): Promise<nraReportsResponseData | undefined> => {
  const response = await Axios.post(reportrUrl, params, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response.data.data;
};
