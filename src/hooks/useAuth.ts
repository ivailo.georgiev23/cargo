import { useContext, useEffect } from "react";
import { useSessionStorage } from "./useSessionStorage";
import { User } from "../ts/types/userRequest";
import { loginApi } from "../api";
import { AuthContext } from "../context/AuthContext";

export const useAuth = () => {
  const { login } = useContext(AuthContext);

  const { getItem } = useSessionStorage();

  useEffect(() => {
    const user = getItem("token");
    if (user) {
      console.log(user);
    }
  }, []);

  const userLogin = async (params: User) => {
    try {
      const response = await loginApi(params);
      if (response.token) {
        login(response.token);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const logout = () => {};

  return { userLogin, logout };
};
