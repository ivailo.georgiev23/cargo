export interface nraReportDataType {
  id: number;
  intId: string;
  status: string;
  payment: string;
  payType: string;
  sendDate: string;
  deliveryDate: string;
  count: number;
  weight: number;
  volume: number;
  zone: string;
  senderUIC: string;
  senderName: string;
  senderTown: string;
  senderTownZip: string;
  receiverUIC: string;
  receiverName: string;
  receiverTown: string;
  receiverTownZip: string;
  cod: number;
  insurance: number;
  surcharge: number;
  fr: number;
  sum: number;
  sumDsc: number;
  vat: number;
  total: number;
  sumFR: number;
}

export interface nraReportsResponseData {
  data: Array<nraReportDataType>;
  page: number;
  size: number;
  total: number;
}
