import MainLayout from "../layout/main-layout";

function Home() {
  return <MainLayout>
    <p>Home</p>
  </MainLayout>;
}

export default Home;
