import React from "react";

// Importing all created components
import { Box } from "@mui/material";
import NavBar from "../components/layout/nav-bar";
import Footer from "../components/layout/footer";

// Pass the child props
export default function MainLayout({
  children,
}: {
  children: React.ReactElement;
}) {
  return (
    <Box display="flex" flexDirection="column" flexGrow={1}>
      <NavBar></NavBar>
      <Box flexGrow={1} margin="auto" maxWidth={1440}>
        {children}
      </Box>
      <Footer />
    </Box>
  );
}
