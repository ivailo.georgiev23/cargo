import { Outlet, Navigate } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { useContext } from "react";

const PrivateRoutes = () => {
  const { isLoggedIn } = useContext(AuthContext);

  console.log(isLoggedIn)
  if (!isLoggedIn) return <Navigate to="/login" replace />;

  return <Outlet />;
};

export default PrivateRoutes;
