import { downloadFile } from "../api";
import { reportRequst } from "../ts/types/reportNraRequest";
import { useSessionStorage } from "./useSessionStorage";

export const useDownload = () => {
  const { getItem } = useSessionStorage();

  const downloadReport = async (url: string, params: reportRequst) => {
    const sessionStorage = getItem("token");
    return downloadFile(url, sessionStorage, params);
  };

  return { downloadReport };
};
