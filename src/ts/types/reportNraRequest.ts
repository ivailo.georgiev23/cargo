export type reportNraRequest = {
  quarter: number;
  year: number;
  domestic: boolean;
  international: boolean;
  cod: boolean;
  page: number;
  size: number;
  sortBy?: string;
  sortDir?: string;
};

export type reportRequst = {
  quarter: number,
  year: number,
  domestic: boolean,
  international: boolean,
  cod: boolean,
}